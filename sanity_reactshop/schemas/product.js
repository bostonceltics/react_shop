export default {
  name: "product",
  title: "Product",
  type: "document",
  fields: [
    {
      name: "image",
      title: "Image",
      type: "array",
      of: [{ type: "image" }],
      options: {
        hotspot: true,
      },
    },
    {
      name: "name",
      title: "Name",
      type: "string",
    },
    {
      name: "slug",
      title: "Slug",
      type: "slug",
      options: {
        source: "name",
        maxLength: 90,
      },
    },
    {
      name: "price",
      title: "Price",
      type: "number",
      validation: (Rule) => Rule.required().warning(`This field is required!`),
    },
    {
      name: "details",
      title: "Details",
      type: "string",
    },
    {
      name: "repeater",
      title: "Repeater",
      type: "array",
      of: [
        {
          type: "object",
          name: "tabs",
          fields: [
            {
              title: "Tab title",
              name: "tabs_item",
              type: "string",
            },
            {
              title: "Tab detail",
              name: "tabs_item_detail",
              type: "text",
            },
          ],
        },
      ],
    },
  ],
};
